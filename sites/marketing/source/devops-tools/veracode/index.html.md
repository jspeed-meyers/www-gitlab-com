---
layout: secure_and_protect_competitors
title: "GitLab vs Veracode"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

<div class="comparison-table comparison-page-content secure-and-protect" markdown="1">

## Summary

Both Veracode and GitLab Ultimate offer open source component scanning along with Static and Dynamic Application Security Testing. Veracode is a mature product with a hefty price tag. Veracode offers a separate SAST-lite product that integrates in the developer's IDE offering spell-check-like functionality to flag vulnerabilities as the developer types.

GitLab Ultimate automatically includes broad security scanning with every code commit including Static and Dynamic Application Security Testing, along with dependency scanning, container scanning, and license management.

Note: In November 2018, the private equity firm Thoma Bravo acquired Veracode from Broadcom.  Veracode now functions as an independent company within the Thoma Bravo portfolio of companies.  Between March 2017 and July 2018 Veracode was part of CA Technologies.  For a brief period, from July 2018 to November 2018, Veracode was part of Broadcom following CA Technologies' acquisition by Broadcom

## Comparison to GitLab

Veracode is a well established player in the Application Security Testing (AST) market.  Although they offer a range of products, including SAST, DAST, IAST, and SCA, each of these products are sold and licensed separately.  GitLab offers simplicity and a high level of integration by including all of these types of scanning capabilities within a single product.  Additionally, GitLab has tightly integrated the scanning results with the rest of the SLDC, including the merge request review process.

Additionally, organizations that have concerns about using a cloud-hosted scanning solution, or that use GitLab’s self managed offering, will find that GitLab is a clear winner as Veracode does not have an on-premise offering.


## Security Scanning

### Strengths and Weaknesses

</div>
<div class="comparison-table comparison-page-content secure-and-protect strengths-and-weaknesses" markdown="1">

| | <b>GitLab</b> | <b>Veracode</b> |
| --- | --- | --- |
| <b>Strengths</b> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tight out-of-the-box integration with the rest of the SDLC, including the merge request review process</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GitLab’s recent acquisitions of Peach Tech and Fuzzit provide Fuzzing capabilities that Veracode lacks</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Supports on-premise deployments including disconnected, offline, or air-gapped environments</span> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Strong offering across scanning types</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nice, clean UX and design</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Strong offering across scanning types</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tight integration with IDEs</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Strong offering across scanning types</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;False positive rates are better than average</span> |
| <b>Weaknesses</b> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GitLab’s SAST offering only scans code repositories today and cannot scan compiled binaries</span> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Only available as a SaaS tool and cannot be deployed on-premise</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Not natively integrated into merge requests or the SDLC</span> |

</div>
<div class="comparison-table comparison-page-content secure-and-protect" markdown="1">

### Feature Lineup

</div>
<div class="comparison-table comparison-page-content secure-and-protect feature-lineup" markdown="1">

| | <b>GitLab</b> | <b>Veracode</b> |
| --- | :-: | :-: |
| SAST | &#9989; | &#9989; |
| DAST | &#9989; | &#9989; |
| IAST | | &#9989; |
| SCA: Vulnerability Scanning | &#9989; | &#9989; |
| SCA: Open Source Audit | &#9989; | &#9989; |
| Fuzz Testing | &#9989; | |

</div>
