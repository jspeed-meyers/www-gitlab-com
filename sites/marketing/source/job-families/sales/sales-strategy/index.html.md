---
layout: job_family_page
title: "Sales Strategy Roles"
description: "The Sales Strategy Job Family helps develop and execute GitLab's Global Sales Strategy."
---

The Sales Strategy Job Family helps develop and execute GitLab's Global Sales Strategy. The Sales Strategy Job Family partners close with Sales Leadership to identify opportunities and improve efficiencies.

### Manager, Sales Strategy

The Manager, Sales Strategy reports to the Senior Director, Sales Strategy.

#### Manager, Sales Strategy Job Grade

The Manager, Sales Strategy is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Sales Strategy Responsibilities

* Develop and execute a Global Sales Strategy by working with Sales Leadership and GTM teams including owning and driving key initiatives and projects
* Partner with Sales Leadership to formulate and develop regional/segment business plans and constantly evolving go-to-market strategies 
* Identify opportunities to improve go-to-market efficiencies and lead efforts to scale, align and invest in the business 
* Analyze and present recommendations to Sales Leadership for alignment and investment in the desired state and opportunities for sales productivity improvement
* Developing an understanding of and staying current with the competitive landscape
* Assess and drive innovative pricing strategies and options for Field Teams 
* Authoring and delivering high-impact presentations and plans to support CRO and internal Sales events 
* Assist with annual planning with Sales Leadership and key GTM Leaders 
* Drive quarterly planning with Theater Sales Leaders and provide analytical support  
* Develop and manage executive reporting on key metrics, formulate actionable insights and structure a concise, clear presentation of findings and prioritize issues as appropriate 
* Ability to conduct sophisticated and creative analysis, yet translate those results to easily digestible messages, communications, and presentations
* Support strategy for Customer Success and Channel Teams
* Be a trusted advisor to Sales Leadership 

#### Manager, Sales Strategy Requirements

* BA/BS degree
* 3+ years of experience in an analytical role within a technology business. Preference for Sales Strategy, Business Intelligence/Analytics, Management Consulting, Venture Capital/Private Equity, and/or Investment Banking backgrounds
* Excellent quantitative analytical skills, creativity in problem solving, and a keen business sense
* Ability to think strategically, but also have exceptional attention to detail to drive program management and execution 
* Extensive track record of building high-quality and complex spreadsheets, models and presentations
* Superb analytical skills and technical aptitude 
* Experience with SQL, Tableau, and/or similar analytical packages a plus
* SaaS and B2B experience preferred
* Interest in GitLab, and open source software
* You share our values, and work in accordance with those values
* Ability to thrive in a fully remote organization
* You share our values, and work in accordance with those values.
* [Leadership at GitLab](/company/team/structure/#director-group)
* Ability to use GitLab

## Specialties

### Channel

* 2+ years experience focused directly with supporting Channel sales organizations

### Enterprise

* 2+ years experience focused directly with supporting Enterprise sales organizations

### Commercial

* 2+ years experience focused directly with supporting Commercial sales organizations

### Senior Manager, Sales Strategy

The Senior Manager, Sales Strategy reports to the Senior Director, Sales Strategy.

#### Senior Manager, Sales Strategy Job Grade 

The Senior Manager, Sales Strategy is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Manager, Sales Strategy Responsibilities

* Extends the Manager, Sales Strategy Responsibilities

#### Senior Manager, Sales Strategy Requirements

* Extends the Manager, Sales Strategy Requirements
* 5+ years of experience in an analytical role within a technology business. Preference for Sales Strategy, Business Intelligence/Analytics, Management Consulting, Venture Capital/Private Equity, and/or Investment Banking backgrounds

### Senior Director, Sales Strategy

The Senior Director, Sales Strategy reports to the [VP, Field Operations](sales/vp-of-field-operations/).

#### Senior Director, Sales Strategy Job Grade 

The Senior Director, Sales Strategy is a [grade 11](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Director, Sales Strategy Responsibilities

* Develop and execute a Global Sales Strategy by working with Sales Leadership and GTM teams including owning and driving key initiatives and projects
* Partner with Sales Leadership to formulate and develop regional/segment business plans and constantly evolving go-to-market strategies 
* Identify opportunities to improve go-to-market efficiencies and lead efforts to scale, align and invest in the business 
* Analyze and present recommendations to Sales Leadership for alignment and investment in the desired state and opportunities for sales productivity improvement
* Developing an understanding of and staying current with the competitive landscape
* Assess and drive innovative pricing strategies and options for Field Teams 
* Authoring and delivering high-impact presentations and plans to support CRO and internal Sales events 
* Assist with annual planning with Sales Leadership and key GTM Leaders 
* Drive quarterly planning with Theater Sales Leaders and provide analytical support  
* Develop and manage executive reporting on key metrics, formulate actionable insights and structure a concise, clear presentation of findings and prioritize issues as appropriate 
* Ability to conduct sophisticated and creative analysis, yet translate those results to easily digestible messages, communications, and presentations
* Support strategy for Customer Success and Channel Teams
* Be a trusted advisor to Sales Leadership 

#### Senior Director, Sales Strategy Requirements

* BA/BS degree, MBA Preferred 
* 8-10+ years of experience in an analytical role within a technology business.  Preference for Strategy Consulting, Corporate Strategy, Venture Capital/Private Equity, and/or Investment Banking backgrounds
* Excellent quantitative analytical skills, creativity in problem solving, and a keen business sense
* Ability to think strategically, but also have exceptional attention to detail to drive program management and execution 
* Extensive track record of building high-quality and complex spreadsheets, models and presentations
* Superb analytical skills, technical aptitude and executive presence
* Experience with SQL, Tableau, and/or similar analytical packages a plus
* SaaS and B2B experience preferred
* Interest in GitLab, and open source software
* You share our values, and work in accordance with those values
* Ability to thrive in a fully remote organization
* You share our values, and work in accordance with those values.
* [Leadership at GitLab](/company/team/structure/#director-group)
* Ability to use GitLab

## Performance Indicators

* [IACV vs. plan > 1](/handbook/sales/performance-indicators/#iacv-vs-plan)
* [IACV efficiency > 1.0](/handbook/sales/performance-indicators/#iacv-efficiency)
* [Win rate > 30%](/handbook/sales/performance-indicators/#win-rate)

## Career Ladder 

The next step in the Sales Strategy job family is not yet defined at GitLab. 

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

1. Phone screen with a GitLab Recruiting Team Member 
2. Video Interview with the Hiring Manager
3. Team Interviews with 1-4 Team Members

Additional details about our process can be found on our [hiring page](/handbook/hiring).